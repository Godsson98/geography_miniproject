package geography_miniproject;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public abstract class GovernedRegion {
	
	public static final String PROP_SEPARATOR = ";";
	
	private int ID;
	private String name;
	private String capitol;
	private int area;
	private int population;
	private String comments;
	
	private static int highestID = -1;
	
	protected static int getNextID() {
		return ++highestID;
	}
	
	public GovernedRegion(String name, String capitol, int area, int population, String comments) {
		this.ID = getNextID();
		this.name = name;
		this.capitol = capitol;
		this.area = area;
		this.population = population;
		this.comments = comments;

	}
	
	@Override
	public String toString() {
		//return semicolon separated property values
		//e.g.: 42;Switzerland;420;8000000
		return String.join(PROP_SEPARATOR, String.valueOf(ID), name, capitol, String.valueOf(area), String.valueOf(population), comments);
	}
	
	// --- Getters and Setters ---
	public void setID(int iD) {
		ID = iD;
	}
	
	public int getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCapitol() {
		return capitol;
	}
	
	public int getArea() {
		return area;
	}
	
	public int getPopulation() {
		return population;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setCapitol(String capitol) {
		this.capitol = capitol;
	}
	
	public void setArea(int area) {
		this.area = area;
	}
	
	public void setPopulation(int population) {
		this.population = population;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}

}