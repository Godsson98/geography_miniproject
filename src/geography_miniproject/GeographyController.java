package geography_miniproject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import geography_miniproject.Country.GovernmentForm;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.converter.IntegerStringConverter;

public class GeographyController {
	private GeographyView view;
	private GovernedRegionModel model;
	private FileManager fileManager;

	private ObservableList<GovernedRegion> govs;

	private boolean CountryPopValid = false;
	private boolean CountryAreaValid = false;
	private boolean StatePopValid = false;
	private boolean StateAreaValid = false;

	// boolean to check if object is new or should be modofied
	public boolean isNew = false;

	public GeographyController(GovernedRegionModel model, GeographyView view) {
		this.model = model;
		this.view = view;
		govs = FXCollections.observableArrayList();

		// set list view action
		view.listView.setOnMouseClicked(this::loadAtributes);

		// set menu item and button actions
		view.home.setOnAction(this::goToHome);
		view.quit.setOnAction(this::quit);
		view.mCountry.setOnAction(this::goToCountry);
		view.mState.setOnAction(this::goToState);
		
		view.btnHome.setOnAction(this::goToHome);
		view.btnState.setOnAction(this::goToState);
		view.btnCountry.setOnAction(this::goToCountry);

		view.txtCountryPop.textProperty().addListener((observable, oldValue, newValue) -> {
			validateCountryPop(newValue);
		});

		view.txtCountryArea.textProperty().addListener((observable, oldValue, newValue) -> {
			validateCountryArea(newValue);
		});

		view.txtStatePop.textProperty().addListener((observable, oldValue, newValue) -> {
			validateStatePop(newValue);
		});

		view.txtStateArea.textProperty().addListener((observable, oldValue, newValue) -> {
			validateStateArea(newValue);
		});

		// initially go to home
		goToHome();
		view.stage.sizeToScene();

	}

	private void validateCountryPop(String newValue) {
		boolean valid = isValidCountryPop(newValue);
		// remove styling
		view.txtCountryPop.getStyleClass().remove("valid");
		view.txtCountryPop.getStyleClass().remove("invalid");
		// add styling
		if (valid) {
			view.txtCountryPop.getStyleClass().add("valid");
		} else {
			view.txtCountryPop.getStyleClass().add("invalid");
		}
		CountryPopValid = valid;

		enableDisableButton();
	}

	private void validateCountryArea(String newValue) {
		boolean valid = isValidCountryPop(newValue);
		view.txtCountryArea.getStyleClass().remove("valid");
		view.txtCountryArea.getStyleClass().remove("invalid");
		if (valid) {
			view.txtCountryArea.getStyleClass().add("valid");
		} else {
			view.txtCountryArea.getStyleClass().add("invalid");
		}
		CountryAreaValid = valid;
		enableDisableButton();
	}

	private void validateStateArea(String newValue) {
		boolean valid = isValidCountryPop(newValue);
		view.txtStateArea.getStyleClass().remove("valid");
		view.txtStateArea.getStyleClass().remove("invalid");
		if (valid) {
			view.txtStateArea.getStyleClass().add("valid");
		} else {
			view.txtStateArea.getStyleClass().add("invalid");
		}
		StateAreaValid = valid;
		enableDisableButton();
	}

	private void validateStatePop(String newValue) {
		boolean valid = isValidCountryPop(newValue);
		view.txtStatePop.getStyleClass().remove("valid");
		view.txtStatePop.getStyleClass().remove("invalid");
		if (valid) {
			view.txtStatePop.getStyleClass().add("valid");
		} else {
			view.txtStatePop.getStyleClass().add("invalid");
		}
		StatePopValid = valid;
		enableDisableButton();
	}

	private void enableDisableButton() {
		boolean valid = true;
		if (CountryPopValid & CountryAreaValid || StatePopValid & StateAreaValid) {
			view.btnSave.setDisable(!valid);
			view.btnDelete.setDisable(!valid);
		} else {
			view.btnSave.setDisable(valid);
			view.btnDelete.setDisable(valid);
		}
	}

	protected boolean isValidCountryPop(String newValue) {
		boolean valid = true;

		try {
			int value = Integer.parseInt(newValue);
			if (value < 1 || value > 2000000000)
				valid = false;
		} catch (NumberFormatException e) {
			valid = false;
		}

		return valid;
	}

	protected boolean isValidCountryArea(String newValue) {
		boolean valid = true;

		try {
			int value = Integer.parseInt(newValue);
			if (value < 1 || value > 2000000000)
				valid = false;
		} catch (NumberFormatException e) {
			valid = false;
		}

		return valid;
	}

	protected boolean isValidStatePop(String newValue) {
		boolean valid = true;

		try {
			int value = Integer.parseInt(newValue);
			if (value < 1 || value > 2000000000)
				valid = false;
		} catch (NumberFormatException e) {
			valid = false;
		}

		return valid;
	}

	protected boolean isValidStateArea(String newValue) {
		boolean valid = true;

		try {
			int value = Integer.parseInt(newValue);
			if (value < 1 || value > 2000000000)
				valid = false;
		} catch (NumberFormatException e) {
			valid = false;
		}

		return valid;
	}

	public void loadAtributes(MouseEvent e) {
		GovernedRegion gov;
		// get object from model
		gov = view.listView.getSelectionModel().getSelectedItem();
		if (gov != null) {
			System.out.println(gov.toString());

			model.set(gov);

			// boolean to set if object is new (every time the list is clicked, the value is
			// not new)
			isNew = false;

			// set button text
			view.btnSave.setText("Save changes");
		}
	}

	public void goToCountry(ActionEvent e) {
		goToCountry();
	}

	public void goToCountry() {
		// set title
		view.lblTitel.setText("Country");
		// set Button text
		view.btnSave.setText("Save");

		// boolean to set if object is new
		addNew();
		
		enableDisableButton();

		view.cardsFields.getStyleClass().remove("welcomeCenter");
		view.cardsButtons.getStyleClass().remove("welcomeCenter2");
		view.cardsFields.getStyleClass().add("Center");
		view.cardsButtons.getStyleClass().add("Center2");
		
		// clear and fill contents
		view.cardsFields.getChildren().clear();
		view.rightDisplay.getChildren().clear();
		view.cardsFields.getChildren().add(view.cardFieldsCountry);
		view.rightDisplay.getChildren().add(view.rightPane);

		// clear and fill button place holder
		view.cardsButtons.getChildren().clear();
		view.cardsButtons.getChildren().add(view.cardControls);

		// set two button functionalities
		view.btnSave.setOnAction(this::saveCountry);
		view.btnDelete.setOnAction(this::delete);

		// size stage to scene
		view.stage.sizeToScene();

		// set new type of model and file manager
		model = new CountryModel();
		fileManager = new CountryFileManager();

		// binding the text fields to the model
		view.txtCountryName.textProperty().bindBidirectional(model.nameProperty());
		view.txtCountryCap.textProperty().bindBidirectional(model.capitolProperty());
		view.txtCountryArea.textProperty().bindBidirectional(model.areaProperty(), new IntegerStringConverter());
		view.txtCountryPop.textProperty().bindBidirectional(model.popProperty(), new IntegerStringConverter());
		view.cmbType.valueProperty().bindBidirectional(((CountryModel) model).govFormProperty());
		view.txtCountryLang.textProperty().bindBidirectional(((CountryModel) model).languageProperty());
		view.txtCommentCountry.textProperty().bindBidirectional(model.commentsProperty());

		view.listView.setCellFactory(view.getListViewCellFactory());

		// fill displayed list
		fillList();
	}

	public void goToState(ActionEvent e) {
		goToState();
	}

	public void goToState() {
		// set title
		view.lblTitel.setText("State");
		// set Button text
		view.btnSave.setText("Save");

		// boolean to set if object is new = true
		addNew();
		//disable save button
		enableDisableButton();

		view.cardsFields.getStyleClass().remove("welcomeCenter");
		view.cardsButtons.getStyleClass().remove("welcomeCenter2");
		view.cardsFields.getStyleClass().add("Center");
		view.cardsButtons.getStyleClass().add("Center2");
		
		// clear and add fields
		view.cardsFields.getChildren().clear();
		view.rightDisplay.getChildren().clear();
		view.cardsFields.getChildren().add(view.cardFieldsState);
		view.rightDisplay.getChildren().add(view.rightPane);
		

		// clear and add buttons place holder
		view.cardsButtons.getChildren().clear();
		view.cardsButtons.getChildren().add(view.cardControls);

		// set button functionalities
		view.btnSave.setOnAction(this::saveState);
		view.btnDelete.setOnAction(this::delete);

		// size stage to scene
		view.stage.sizeToScene();

		// set new type of model and file manager
		model = new StateModel();
		fileManager = new StateFileManager();

		try {
			fillCombobox();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// binding text fields to model
		view.cmbCountry.valueProperty().bindBidirectional(((StateModel) model).countryProperty());
		view.txtStateName.textProperty().bindBidirectional(model.nameProperty());
		view.txtStateCap.textProperty().bindBidirectional(model.capitolProperty());
		view.txtStateArea.textProperty().bindBidirectional(model.areaProperty(), new IntegerStringConverter());
		view.txtStatePop.textProperty().bindBidirectional(model.popProperty(), new IntegerStringConverter());
		view.txtCommentState.textProperty().bindBidirectional(model.commentsProperty());

		view.listView.setCellFactory(view.getListViewCellFactory());

		// place all lines from file into list
		fillList();

	}

	public void fillCombobox() throws IOException {
		// clear combo-box
		view.cmbCountry.getItems().clear();
		// file to be read by stream
		FileManager fileMan = new CountryFileManager();
		File file = new File("countries.txt");
		
		// list to add the country names
		List<String> list = new ArrayList<String>();
		
		try {
			Stream<String> stream = Files.lines(file.toPath()); // read file into stream
			stream.forEach(l -> {
				GovernedRegion gov = fileMan.fromProperties(l.split(GovernedRegion.PROP_SEPARATOR));  // create an object from all the lines
				if (gov != null) {
					list.add(gov.getName());  // if not object not blank, add name to list
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		// fill combo-box
		view.cmbCountry.getItems().addAll(list);
	}

	// add to listener to back button in the menu
	public void goToHome(ActionEvent e) {
		goToHome();
	}

	public void goToHome() {
		// preparing scene
		view.cardsFields.getStyleClass().remove("Center");
		view.cardsButtons.getStyleClass().remove("Center2");
		
		view.lblTitel.setText("Welcome");
		view.cardsFields.getChildren().clear();
		view.cardsButtons.getChildren().clear();
		view.rightDisplay.getChildren().clear();
		
		view.cardsFields.getStyleClass().add("welcomeCenter");
		view.cardsButtons.getStyleClass().add("welcomeCenter2");
		
		String text = "This application will help you "
				+ "manage the countries and states "
				+ "which you have visited, or gathered"
				+ " information from. Please chose the "
				+ "governed region you would like to create! \n";
		String note =  "note: \n"
				+ "Take into account, to create a state, a country "
				+ "must be registered in advance.";
		
		Label welcomeText = new Label(text);
		welcomeText.setWrapText(true);
		welcomeText.setAlignment(Pos.CENTER);
		welcomeText.getStyleClass().removeAll();
		welcomeText.getStyleClass().add("welcomeText");
		
		Label welcomeNote = new Label(note);
		welcomeNote.setWrapText(true);
		welcomeNote.getStyleClass().removeAll();
		welcomeNote.getStyleClass().add("welcomeNote");
		
		VBox welcome  = new VBox();
		welcome.getChildren().addAll(welcomeText, welcomeNote);
		
		view.cardsButtons.getChildren().add(view.cardMenu);
		view.cardsFields.getChildren().add(welcome);
		view.stage.sizeToScene();

		// clear model and file manager
		model = null;
		fileManager = null;

		clearValues();
	}

	public boolean addNew() {
		view.btnSave.setText("Save");
		return isNew = true;
	}

	public void saveState(ActionEvent e) {
		saveState();
	}

	public void saveState() {
		StateFileManager fileManager = new StateFileManager();
		State state;

		if (isNew) { // create new state
			String name = model.getNameProperty();
			String capitol = model.getCapitolProperty();
			int area = model.getAreaProperty();
			int pop = model.getPopProperty();
			String country = ((StateModel) model).getCountryProperty();
			String comments = model.getCommentsProperty();
			state = new State(name, capitol, area, pop, country, comments);
		} else {  //get existing state from model, and modify attributes
			state = (State) model.getProperty();
			state.setName(model.getNameProperty());
			state.setCapitol(model.getCapitolProperty());
			state.setArea(model.getAreaProperty());
			state.setPopulation(model.getPopProperty());
			state.setCountry(((StateModel) model).getCountryProperty());
			state.setComments(model.getCommentsProperty());
		}

		fileManager.save(state, isNew);  // save into file

		// change boolean back to default value
		isNew = false;

		// TODO fill side bar with state names
		fillList();
		
		// go to initial state state
		goToState();
		
		// clear values from view
		clearValues();
	}

	public void saveCountry(ActionEvent e) {
		saveCountry();
	}

	public void saveCountry() {
		CountryFileManager fileManager = new CountryFileManager();
		Country country;

		if (isNew) {  // create new country
			String name = model.getNameProperty();
			String capitol = model.getCapitolProperty();
			int area = model.getAreaProperty();
			int population = model.getPopProperty();
			GovernmentForm governmentForm = ((CountryModel) model).getGovForm();
			String language = ((CountryModel) model).getLanguage();
			String comments = model.getCommentsProperty();
			country = new Country(name, capitol, area, population, governmentForm, language, comments);
		} else {  // get country from model and modify attributes
			country = (Country) model.getProperty();
			country.setName(model.getNameProperty());
			country.setCapitol(model.getCapitolProperty());
			country.setArea(model.getAreaProperty());
			country.setPopulation(model.getPopProperty());
			country.setGF(((CountryModel) model).getGovForm());
			country.setLanguage(((CountryModel) model).getLanguage());
			country.setComments(model.getCommentsProperty());
		}
		// save country to file
		fileManager.save(country, isNew);
		
		// change boolean value to default 
		isNew = false;
		
		// fill sidebar with country names
		fillList();

		// go to initial country state
		goToCountry();
		

		// clear values from view
		clearValues();
	}

	public void fillList() {
		govs.clear();
		govs.addAll(fileManager.getAll());
		view.listView.setItems(govs);
	}

	public void delete(ActionEvent e) {
		delete();
	}

	// non-specific delete handler
	public void delete() {
		fileManager.delete(model.getProperty());
		fillList();
		clearValues();

		// set Button text
		view.btnSave.setText("Save");
	}
	
	// clear view
	public void clearValues() {
		view.txtCountryName.clear();
		view.txtCountryCap.clear();
		view.txtCountryArea.clear();
		view.txtCountryPop.clear();
		view.txtCountryLang.clear();
		view.txtCommentCountry.clear();
		view.cmbType.setValue(null);

		view.txtStateName.clear();
		view.txtStateCap.clear();
		view.txtStateArea.clear();
		view.txtStatePop.clear();
		view.txtCommentState.clear();
		view.cmbCountry.setValue(null);
	}
	
	public void quit(ActionEvent e) {
		view.stage.close();
	}
	
}
