package geography_miniproject;

import javafx.beans.binding.BooleanBinding;

public class BooleanBindingFields extends BooleanBinding {

	private GeographyView view;
	
	public BooleanBindingFields(GeographyView view) {
		this.view = view;
	}
	@Override
	protected boolean computeValue() {
		boolean valid = false;
		
		view.txtCountryName.textProperty().isEmpty();
		view.txtCountryArea.textProperty().isEmpty();
		//view.txtPop.textProperty().isEmpty();
		view.cmbCountry.valueProperty().isNull();
		//view.txtLang.textProperty().isEmpty();
		
		return valid;
	}

}
