package geography_miniproject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class FileManager {
	protected GeographyView view;

	protected String filePath;
	protected static int ID;

	// setting the file path (countries or states)
	public FileManager(String filePath) {
		this.filePath = filePath;
	}

	// add all elements from list to a GovernedRegion list
	public List<GovernedRegion> getAll() {
		List<GovernedRegion> list = new ArrayList<GovernedRegion>(); // Governed Region list returned

		try {
			// creating a Stream from the file contents
			Stream<String> lines = loadFileContent();
			// for each line do
			lines.forEach(l -> {
				// Create new GovernedRegion
				GovernedRegion gov =
						// define GovernedRegion type according to number and type of elements
						fromProperties(
								// split the line using the separator character
								l.split(GovernedRegion.PROP_SEPARATOR));
				if (gov != null) {
					// add to list
					list.add(gov);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("govs loaded: " + list.size());

		return list;
	}

	// creating an array of String from the line to identify object type
	// (implemented in concrete classes)
	protected abstract GovernedRegion fromProperties(String[] properties);

	//
	public void save(GovernedRegion gov, boolean isNew) {
		// getting ID of the GovernedRegion
		String ID = String.valueOf(gov.getID());
		try {
			// create Stream from file contents
			Stream<String> lines = loadFileContent();
			List<String> list;

			if (isNew) {
				// convert stream of lines to list ("bottle water")
				list = lines.collect(Collectors.toList());
				// add new line containing object properties to list
				list.add(gov.toString());
			} else {
				// TODO
				// go through each line
				list = lines.map(line -> {
					// find line beginning with our ID
					if (line.startsWith(ID)) {
						// overwrite line with new data
						line = gov.toString();
					}
					return line;
					// convert stream of lines to list (bottle water)
				}).collect(Collectors.toList());
			}
			// re-write file
			write(list);
		} catch (IOException e) {
			System.err.println("could not save " + gov.getClass().getSimpleName() + ": " + gov.toString());
			e.printStackTrace();
		}

	}

	// load file content
	private Stream<String> loadFileContent() throws IOException {
		// give me the lines from this file at the file path
		return Files.lines(getFile().toPath())
				// and then filter out blank lines
				.filter(l -> !l.isBlank());
	}

	// creating (if doesn't exist) or returning file
	private File getFile() throws IOException {
		File file = new File(this.filePath);
		// creates new file only when filename doesn't exist
		file.createNewFile();
		return file;
	}

	// fill file with the list of strings
	private void write(List<String> lines) throws IOException {
		// truncate (empty) file and fill it with lines (list of string)
		Files.write(getFile().toPath(), lines, StandardOpenOption.TRUNCATE_EXISTING);
	}

	// delete object from file
	public void delete(GovernedRegion gov) {
		int ID = gov.getID();
		// System.out.println("delete " + gov.getClass().getSimpleName() + ": " + ID);

		try {
			// get content of file..
			write(loadFileContent()
					// ..filter out lines beginning with given ID..
					.filter(l -> !lineBeginsWithID(l, ID)).collect(Collectors.toList())); // ...and overwrite file with
																							// write()

		} catch (IOException e) {
			System.err.println("could not delete " + gov.getClass().getSimpleName() + ": " + gov.toString());
			e.printStackTrace();
		}
	}

	private boolean lineBeginsWithID(String line, int ID) {
		// split line by separator and check if first element is equal to ID
		return line.split(GovernedRegion.PROP_SEPARATOR)[0].equals(String.valueOf(ID));
	}

//	public ArrayList<GovernedRegion> addCountryToPhantomList(String filePath) {
//		ArrayList<GovernedRegion> list = new ArrayList<GovernedRegion>();
//
//		try {
//			Stream<String> lines = loadFileContent();
//			lines.forEach(l -> {
//				GovernedRegion gov = fromProperties(l.split(GovernedRegion.PROP_SEPARATOR));
//				if (gov != null) {
//					list.add(gov);
//				}
//			});
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}
//
//	public ArrayList<String> addCountryToNameList(String filePath) {
//		ArrayList<String> list = new ArrayList<String>();
//		
//		try {
//			Stream<String> lines = loadFileContent();
//			lines.forEach(l -> {
//				GovernedRegion gov = fromProperties(l.split(GovernedRegion.PROP_SEPARATOR));
//				if (gov != null) {
//					list.add(gov.getName());
//				}
//			});
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}
}
