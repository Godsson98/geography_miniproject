package geography_miniproject;

import javafx.beans.property.SimpleObjectProperty;

public abstract class GovernedRegionModel {

	private final SimpleObjectProperty<GovernedRegion> property = new SimpleObjectProperty<>();
	private final SimpleObjectProperty<String> nameProperty = new SimpleObjectProperty<>();
	private final SimpleObjectProperty<String> capitolProperty = new SimpleObjectProperty<>();
	private final SimpleObjectProperty<Integer> areaProperty = new SimpleObjectProperty<>();
	private final SimpleObjectProperty<Integer> popProperty = new SimpleObjectProperty<>();
	private final SimpleObjectProperty<String> commentsProperty = new SimpleObjectProperty<>();
	
	public void save(GovernedRegion gov) {
		set(gov);
	}

	public void delete() {
		setProperty(null);
		setNameProperty(null);
		setCapitolProperty(null);
		setAreaProperty((Integer) 0);
		setPopProperty((Integer) 0);
		setCommentsProperty(null);
	}
	
	public void set(GovernedRegion gov) {
		property.set(gov);
		nameProperty.set(gov.getName());
		capitolProperty.set(gov.getCapitol());
		areaProperty.set(gov.getArea());
		popProperty.set(gov.getPopulation());
		commentsProperty.set(gov.getComments());
	}
	
	public SimpleObjectProperty<GovernedRegion> property() {
		return property;
	}
	
	public SimpleObjectProperty<String> nameProperty() {
		return nameProperty;
	}
	
	public SimpleObjectProperty<String> capitolProperty() {
		return capitolProperty;
	}
	
	public SimpleObjectProperty<Integer> areaProperty() {
		return areaProperty;
	}
	
	public SimpleObjectProperty<Integer> popProperty() {
		return popProperty;
	}
	
	public SimpleObjectProperty<String> commentsProperty() {
		return commentsProperty;
	}
	
	protected void setProperty(GovernedRegion gov) {
		property.set(gov);
	}
	
	public void setNameProperty(String name) {
		nameProperty.set(name);
	}
	
	public void setCapitolProperty(String name) {
		capitolProperty.set(name);
	}
	
	public void setAreaProperty(int area) {
		areaProperty.set(area);
	}
	
	public void setPopProperty(int pop) {
		popProperty.set(pop);
	}
	
	public void setCommentsProperty(String comments) {
		commentsProperty.set(comments);
	}

	public GovernedRegion getProperty() {
		return property.get();
	}
	
	public String getNameProperty() {
		return nameProperty.get();
	}
	
	public String getCapitolProperty() {
		return capitolProperty.get();
	}
	
	public Integer getAreaProperty() {
		return areaProperty.get();
	}
	
	public Integer getPopProperty() {
		return popProperty.get();
	}
	
	public String getCommentsProperty() {
		return commentsProperty.get();
	}

}
