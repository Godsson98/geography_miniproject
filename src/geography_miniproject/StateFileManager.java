package geography_miniproject;

public class StateFileManager extends FileManager {

	public StateFileManager() {
		super("states.txt");
	}

	@Override
	protected State fromProperties(String[] properties) {
		State state = null;
		if (properties.length == 7) {
			state = new State(properties[1], // Name
					properties[2], //capitol
					Integer.parseInt(properties[3]), // area
					Integer.parseInt(properties[4]), // population
					properties[6], //country; 
					properties[5]); //comments
			state.setID(Integer.parseInt(properties[0])); //ID
		}
		return state;
	}

}
