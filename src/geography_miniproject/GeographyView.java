package geography_miniproject;

import java.io.IOException;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

public class GeographyView {

	private GovernedRegionModel model;
	public Stage stage;

	// Initial element holder
	public BorderPane root = new BorderPane();

	// group holder panes
	StackPane cardsFields = new StackPane();
	StackPane cardsButtons = new StackPane();
	StackPane cardsDisplay = new StackPane();
	StackPane rightDisplay = new StackPane();

	// element holder groups
	Group cardMenu;
	Group cardFieldsCountry;
	Group cardFieldsState;
	Group cardControls;
	Group rightPane;

	// menu items
	Menu menuFile;
	Menu menuSubfile;
	Menu menuSubSubfile;
	MenuItem home = new MenuItem("Home");
	MenuItem quit = new MenuItem("Quit");
	MenuItem mCountry = new MenuItem("Country");
	MenuItem mState = new MenuItem("State");

	// Toolbar Title
	Label lblTitel = new Label();

	// labels
	private Label lblFooter = new Label("Geography mini-app");

	// fields
	TextField txtCountryName = new TextField("");
	TextField txtCountryCap = new TextField("");
	TextField txtCountryArea = new TextField("");
	TextField txtCountryPop = new TextField("");
	TextField txtCountryLang = new TextField("");

	TextField txtStateName = new TextField("");
	TextField txtStateCap = new TextField("");
	TextField txtStateArea = new TextField("");
	TextField txtStatePop = new TextField("");

	// Text Area
	TextArea txtCommentState = new TextArea("");
	TextArea txtCommentCountry = new TextArea("");

	// field labels
	Label lblCountry = new Label(" Country ");
	Label lblStateName = new Label(" Name ");
	Label lblStateCap = new Label(" Capital ");
	Label lblStateArea = new Label(" Area ");
	Label lblStatePop = new Label(" Population ");
	Label lblStateComment = new Label(" Comments ");

	Label lblCountryName = new Label(" Name ");
	Label lblCountryCap = new Label(" Capital ");
	Label lblCountryArea = new Label(" Area ");
	Label lblCountryPop = new Label(" Population ");
	Label lblType = new Label(" Type ");
	Label lblCountryLang = new Label(" Language ");
	Label lblCountryComment = new Label(" Comment ");

	// spacer
	Region spacer = new Region();
	Region spacerC = new Region();
	Region spacerS = new Region();

	// country
	ComboBox<Country.GovernmentForm> cmbType = new ComboBox<Country.GovernmentForm>();
	// state
	ComboBox<String> cmbCountry = new ComboBox<String>();

	// right pane list
	ListView<GovernedRegion> listView = new ListView<GovernedRegion>();

	// group menu
	Button btnCountry = new Button("Country");
	Button btnState = new Button("State");

	// group controls
	Button btnHome = new Button("Home");
	Button btnDelete = new Button("Delete");
	Button btnSave = new Button("Save");

	public GeographyView(Stage stage, GovernedRegionModel model) throws IOException {
		this.stage = stage;

		// fill combo boxes
		cmbType.getItems().addAll(Country.GovernmentForm.values());

		// Center part element holder
		VBox center = new VBox();

		// Buttons home screen
		HBox cardMenuHBox = new HBox();
		cardMenuHBox.setSpacing(5);
		cardMenuHBox.getChildren().addAll(btnCountry, btnState);

		// set height of txtArea
		txtCommentState.setPrefRowCount(4);
		txtCommentState.setPrefColumnCount(10);
		txtCommentState.setWrapText(true);

		txtCommentCountry.setPrefRowCount(4);
		txtCommentCountry.setPrefColumnCount(10);
		txtCommentCountry.setWrapText(true);

		// State Fields
		GridPane cardFieldsStateGridPane = new GridPane();
		cardFieldsStateGridPane.add(lblCountry, 0, 0);
		cardFieldsStateGridPane.add(cmbCountry, 1, 0);
		cardFieldsStateGridPane.add(lblStateName, 0, 1);
		cardFieldsStateGridPane.add(txtStateName, 1, 1);
		cardFieldsStateGridPane.add(lblStateCap, 0, 2);
		cardFieldsStateGridPane.add(txtStateCap, 1, 2);
		cardFieldsStateGridPane.add(lblStateArea, 0, 3);
		cardFieldsStateGridPane.add(txtStateArea, 1, 3);
		cardFieldsStateGridPane.add(lblStatePop, 0, 4);
		cardFieldsStateGridPane.add(txtStatePop, 1, 4);
		cardFieldsStateGridPane.add(lblStateComment, 0, 5);
		cardFieldsStateGridPane.add(txtCommentState, 1, 5);
		cardFieldsStateGridPane.getStyleClass().add("Centerlabel");

		// spacing and design
		spacerS.setMinWidth(8);
		cardFieldsStateGridPane.add(spacerS, 2, 0, 1, 7);
		cardFieldsStateGridPane.setVgap(5);
		cardFieldsStateGridPane.setMargin(txtCommentState, new Insets(0, 0, 15, 0));

		// Country fields
		GridPane cardFieldsCountryGridPane = new GridPane();
		cardFieldsCountryGridPane.add(lblCountryName, 0, 0);
		cardFieldsCountryGridPane.add(txtCountryName, 1, 0);
		cardFieldsCountryGridPane.add(lblCountryCap, 0, 1);
		cardFieldsCountryGridPane.add(txtCountryCap, 1, 1);
		cardFieldsCountryGridPane.add(lblCountryArea, 0, 2);
		cardFieldsCountryGridPane.add(txtCountryArea, 1, 2);
		cardFieldsCountryGridPane.add(lblCountryPop, 0, 3);
		cardFieldsCountryGridPane.add(txtCountryPop, 1, 3);
		cardFieldsCountryGridPane.add(lblType, 0, 4);
		cardFieldsCountryGridPane.add(cmbType, 1, 4);
		cardFieldsCountryGridPane.add(lblCountryLang, 0, 5);
		cardFieldsCountryGridPane.add(txtCountryLang, 1, 5);
		cardFieldsCountryGridPane.add(lblCountryComment, 0, 6);
		cardFieldsCountryGridPane.add(txtCommentCountry, 1, 6);
		cardFieldsCountryGridPane.getStyleClass().add("Centerlabel");

		HBox hboxTest = new HBox();
		hboxTest.getChildren().addAll(cardFieldsCountryGridPane, cardFieldsStateGridPane);

		// spacing and design
		spacerC.setMinWidth(8);
		cardFieldsCountryGridPane.add(spacerC, 2, 0, 1, 7);
		cardFieldsCountryGridPane.setVgap(6);
		cardFieldsCountryGridPane.setMargin(txtCommentCountry, new Insets(0, 0, 15, 0));

		// Control buttons on Country and State pages
		HBox cardControlsHBox = new HBox();
		cardControlsHBox.setSpacing(5);
		cardControlsHBox.getChildren().addAll(btnHome, btnDelete, btnSave);

		// Placing elements in groups
		cardMenu = new Group(cardMenuHBox);
		cardFieldsCountry = new Group(cardFieldsCountryGridPane);
		cardFieldsState = new Group(cardFieldsStateGridPane);
		cardControls = new Group(cardControlsHBox);
		rightPane = new Group(rightPane());

		// Placing groups in center
		center.getChildren().add(cardsFields);
		center.getChildren().add(cardsButtons);

		// auto resize
		center.prefWidthProperty().bindBidirectional(root.prefWidthProperty());

		root.getStyleClass().add("root");
		// Placing all elements in initial element holder
		root.setTop(Toolbar());
		root.setCenter(center);
		root.setRight(rightDisplay);
		// root.setLeft(leftPane());
		root.setBottom(footer());

		// setting up scene and placing element holder in scene
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("Geography.css").toExternalForm());
		Image icon = new Image(getClass().getResourceAsStream("/image/geo.png"));
		stage.getIcons().add(icon);
		stage.sizeToScene();
		stage.setTitle("Geography");
		stage.setScene(scene);
		stage.show();
	}

	// Start method
	public void start() {
		stage.show();
	}

	private Pane rightPane() {
		VBox pane = new VBox();

		pane.getStyleClass().add("listView");
		pane.getChildren().add(listView);
		return pane;
	}

	// private Pane leftPane() {
	// VBox pane = new VBox();
	// label
	// pane.getChildren().add(listView); - to be defined what the context is
	// pane.getChildren().add(listView2);

	// return pane;
	// }

	private Pane Toolbar() {
		// Menu setup
		MenuBar menuBar = new MenuBar();
		menuFile = new Menu("File");
		menuSubfile = new Menu("Options");
		menuSubSubfile = new Menu("Create");
		menuFile.getItems().addAll(menuSubfile, menuSubSubfile);
		menuSubfile.getItems().add(home);
		menuSubfile.getItems().add(quit);
		menuSubSubfile.getItems().add(mCountry);
		menuSubSubfile.getItems().add(mState);

		menuBar.getMenus().addAll(menuFile);

		// Create spacers
		Region region1 = new Region();
		HBox.setHgrow(region1, Priority.ALWAYS);

		Region region2 = new Region();
		HBox.setHgrow(region2, Priority.ALWAYS);

		lblTitel.getStyleClass().add("CategoryLabel");

		// Create and fill toolbar with menu items
		HBox toolbar = new HBox(menuBar, region1, lblTitel, region2);
		toolbar.getStyleClass().add("hbox");

		return toolbar;

	}

	private Pane footer() {
		// Create spacers
		Region region1 = new Region();
		HBox.setHgrow(region1, Priority.ALWAYS);

		Region region2 = new Region();
		HBox.setHgrow(region2, Priority.ALWAYS);

		// add style to footer
		lblFooter.getStyleClass().add("footer");

		// fill footer
		HBox footer = new HBox(region1, lblFooter, region2);
		footer.getStyleClass().add("footer");
		return footer;
	}

	// https://stackoverflow.com/questions/24597911/how-to-define-which-property-listview-should-use-to-render
	public Callback<ListView<GovernedRegion>, ListCell<GovernedRegion>> getListViewCellFactory() {
		return new Callback<ListView<GovernedRegion>, ListCell<GovernedRegion>>() {

			@Override
			public ListCell<GovernedRegion> call(ListView<GovernedRegion> param) {
				return new ListCell<GovernedRegion>() {
					@Override
					protected void updateItem(GovernedRegion item, boolean empty) {
						super.updateItem(item, empty);
						if (item == null) {
							setText(null);
						} else {
							setText(item.getName());
						}
					}

				};
			}
		};
	}

//	private void loadSplashScreen() {
//
//			StackPane pane = null;
//			try {
//				pane = (StackPane) FXMLLoader.load(getClass().getResource("SplashImage.fxml"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			root.setCenter(pane);
//			
//			FadeTransition fadeIn = new FadeTransition(Duration.seconds(3), pane);
//			fadeIn.setFromValue(0);
//			fadeIn.setToValue(1);
//			fadeIn.setCycleCount(1);
//			
//			fadeIn.play();
//			
//
//	}

}