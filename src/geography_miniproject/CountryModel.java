package geography_miniproject;

import javafx.beans.property.SimpleObjectProperty;

public class CountryModel extends GovernedRegionModel {

	private final SimpleObjectProperty<Country.GovernmentForm> govFormProperty = new SimpleObjectProperty<>();
	private final SimpleObjectProperty<String> languageProperty = new SimpleObjectProperty<>();

	@Override
	public void save(GovernedRegion gov) {
		super.save(gov);
		// save additional fields
		Country country = (Country) gov;
		govFormProperty.set(country.getGF());
		languageProperty.set(country.getLanguage());
	}

	@Override
	public void delete() {
		super.delete();
		// clear additional fields
		setGovFormProperty(null);
		setLanguageProperty(null);
	}
	
	@Override
	public void set(GovernedRegion gov) {
		super.set(gov);
		Country country = (Country) gov;
		govFormProperty.set(country.getGF());
		languageProperty.set(country.getLanguage());
	}

	public SimpleObjectProperty<Country.GovernmentForm> govFormProperty() {
		return govFormProperty;
	}

	public void setGovFormProperty(Country.GovernmentForm gf) {
		govFormProperty.set(gf);

	}

	public SimpleObjectProperty<String> languageProperty() {
		return languageProperty;
	}

	public void setLanguageProperty(String language) {
		languageProperty.set(language);
	}
	
	public Country.GovernmentForm getGovForm() {
		return govFormProperty.get();
	}
	
	public String getLanguage() {
		return languageProperty.get();
	}

}
