package geography_miniproject;

import java.util.ArrayList;
import java.util.List;

public class State extends GovernedRegion {
	
	public String country;
	
	
	public State (String name, String capitol, int area, int population, String country, String comments) {
		super(name, capitol, area, population, comments);
		this.country = country;
		System.out.println("State: " + this.toString());
	}

	@Override
	public String toString() {
		//override super class toString method
		return String.join(PROP_SEPARATOR, super.toString(), country);
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	  
}
