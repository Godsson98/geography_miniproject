package geography_miniproject;

public class Country extends GovernedRegion {

	public enum GovernmentForm {
		Anarchy,
		Aristocracy,
		Bureaucracy,
		Capitalism,
		Colonialism,
		Communism,
		Democracy,
		Federalism,
		Feudalism,
		Kleptocracy,
		Meritocracy,
		Military_Dictatorship,
		Monarchy,
		Oligarchy,
		Plutocracy,
		Republic,
		Socialism,
		Theocracy,
		Totalitarianism,
		Tribalism
	}
	
	private GovernmentForm governmentForm;
	private String language;
	
	public Country(String name, String capitol, int area, int population, GovernmentForm governmentForm, String language, String comments) {
		super(name, capitol, area, population, comments);
		this.governmentForm = governmentForm;
		this.language = language;
		System.out.println("Country: " + this.toString());
	}

	
	@Override
	public String toString() {
		//override super class toString method
		return String.join(PROP_SEPARATOR, super.toString(), governmentForm.toString(), language);
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public GovernmentForm getGF() {
		return governmentForm;
	}
	
	public void setGF(GovernmentForm governmentForm) {
		this.governmentForm = governmentForm;
	}

}
