package geography_miniproject;

import javafx.beans.property.SimpleObjectProperty;

public class StateModel extends GovernedRegionModel {

	// additional fields
	private final SimpleObjectProperty<String> countryProperty = new SimpleObjectProperty<>();

	@Override
	public void save(GovernedRegion gov) {
		super.save(gov);
		// save additional fields
		State state = (State) gov;
		countryProperty.set(state.getCountry());
	}
	
	@Override
	public void delete() {
		super.delete();
		// clear additional fields
		setCountryProperty(null);
	}
	
	@Override
	public void set(GovernedRegion gov) {
		super.set(gov);
		State state = (State) gov;
		setCountryProperty(state.getCountry());
	}
	
	public SimpleObjectProperty<String> countryProperty() {
		return countryProperty;
	}
	
	public void setCountryProperty(String country) {
		countryProperty.set(country);
	}
	
	public String getCountryProperty () {
		return countryProperty.get();
	}
}
