package geography_miniproject;

import javafx.application.Application;
import javafx.stage.Stage;

public class GeographyMVC extends Application {
	
	private GovernedRegionModel model;
	private GeographyController controller;
	private GeographyView view;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		view = new GeographyView(stage, model);
		controller = new GeographyController(model, view);
		view.start();
	}
}
