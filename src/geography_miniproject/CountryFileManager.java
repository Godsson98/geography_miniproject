package geography_miniproject;

import geography_miniproject.Country.GovernmentForm;

public class CountryFileManager extends FileManager {

	public CountryFileManager() {
		super("countries.txt");
	}
	
	@Override
	protected Country fromProperties(String[] properties) {
		Country country = null;
		if (properties.length == 8) {
			country = new Country(properties[1], // Name
					properties[2], //Capitol
					Integer.parseInt(properties[3]), // area
					Integer.parseInt(properties[4]), // population
					GovernmentForm.valueOf(properties[6]), //government form
					properties[7], //language
					properties[5]); //comments
			country.setID(Integer.parseInt(properties[0])); //ID
		}
		return country;
	}
}
