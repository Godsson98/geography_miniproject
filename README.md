# Geography_Miniproject
Students:
Daniel Gergely
Tufan Basaran


General Requirements
1. Abstract class of type Governed Region
2. Concrete classes that extend the Governed Region class
Minimum attributes: Name, government form, area, population (Country for state)
3. Objects need to be stored in an ArrayList.
4. The CRUD of these objects

Our Application
The implementation of an Abstract Class GovernedRegion with two classes which extend this class. Country and State. 
Along the above specified attributes, we have implemented these extra attributes: ID, capital, language and comments. 
In the two integer fields (area and population) we implemented an integer check, which disables the save (and delete) button(s) as long as these fields are empty or have non-integer values in them. 
We also created an abstract models structure, which enables us to separately store and handle the two object types (states and countries).
Along with the basic MVC structure, we also implemented an abstract file controller scenario. This enables us to save the input values of the user in two separate text files. These values are constantly read and updated according to user action. 
When the program is quit, the values of the user are stored in these text files and at the reopening of the program, read back into the application. 
A ListView is used to display the saved objects. One click on one of these elements re-populates the text fields of the app accordingly and enables the user to edit or even delete the object. 
The whole application is based in one stage. The scene and the elements are removed and added according to user interaction. The stage is always set to the size of the scene and its contained elements. 
The application is also equiped with a custom image which appears on the taskbar and in the application window. 
Also a small menu bar provides the user with alternate navigation options.
Last but not least, the whole application is styled with an extensive CSS style sheet using a standardized color sceme. The animation of the buttons and  the list view are also an additional feature.

Extra features summary:
Abstract Model structure
Abstract File Manager structure
Saving of data into a text file
Displaying of values in a List view
A Menu
"Stage switching"
Custom application image
Advanced CSS styling and animation

